package computer2;

public class Processor {

	String manufacturer;
	int coresNumber;
	int frequency;
	int cache;
	
	public void start() {

		System.out.println("Processor: "+ manufacturer+" STARTED!");
		System.out.println("Processor cores: "+ coresNumber);
		System.out.println("Processor frequency: "+ frequency);
		System.out.println("Processor cache: "+ cache);
		
	}
	
}
