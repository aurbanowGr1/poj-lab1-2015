package computer2;

public class GraphicCard {

	String manufacturer;
	String model;
	int memory;
	
	Monitor monitor;

	public void start() {

		monitor.sendPicture("Graphic card: "+ manufacturer + " STARTED!");
		monitor.sendPicture("Graphic card model: "+ model);
		monitor.sendPicture("Graphic card memory: "+ memory);
		
	}
}
