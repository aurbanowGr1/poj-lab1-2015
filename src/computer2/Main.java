package computer2;

public class Main {

	public static void main(String[] args)
	{
		System.out.println("Hello world !");
		
		Mainboard asus = new Mainboard();
		asus.manufacturer="ASUS";
		
		GraphicCard nvidia = new GraphicCard();
		nvidia.manufacturer="GeForce";
		nvidia.memory=1024;
		nvidia.model="Titan";
		
		HardDrive samsung = new HardDrive();
		samsung.manufacturer="Samsung";
		samsung.size=250;
		samsung.speed=7200;
		
		Keyboard logitech = new Keyboard();
		logitech.manufacturer="Logitech";
		
		MemoryRam kingstone = new MemoryRam();
		kingstone.manufacturer="Kingstone";
		kingstone.memory=2048;
		
		Monitor lg = new Monitor();
		lg.manufacturer="LG";
		lg.diagonal=19;
		
		
		Mouse microsoft =new Mouse();
		microsoft.manufacturer="Microsoft";
		
		PowerSource tracer = new PowerSource();
		tracer.manufacturer="Tracer";
		tracer.power=700;
		
		Processor amd = new Processor();
		amd.cache=800;
		amd.coresNumber=4;
		amd.frequency=3;
		amd.manufacturer="AMD";
		
		
		asus.graphicCard=nvidia;
		asus.hardDrives[0] = samsung;
		asus.keyboard=logitech;
		asus.memoryRams[0] = kingstone;
		asus.mouse=microsoft;
		asus.powerSource=tracer;
		asus.processor=amd;
		nvidia.monitor=lg;
		tracer.mainboard=asus;
		
		tracer.powerOn();
		
	}
}
