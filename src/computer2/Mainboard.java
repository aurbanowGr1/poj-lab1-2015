package computer2;

public class Mainboard {

	String manufacturer;
	
	GraphicCard graphicCard;
	HardDrive[] hardDrives = new HardDrive[5];
	Keyboard keyboard;
	MemoryRam[] memoryRams = new MemoryRam[3];
	Mouse mouse;
	PowerSource powerSource;
	Processor processor;

	public void start()
	{
		System.out.println("Mainboard: "+ manufacturer + " Started!");
		graphicCard.start();
		
		processor.start();
		
		for(int i=0;i<memoryRams.length;i++)
		{
			if(memoryRams[i]!=null)
			{
				memoryRams[i].start();
			}
		}
		
		for(int i = 0; i< hardDrives.length; i++)
		{
			
			if(hardDrives[i]!=null)
			{
				hardDrives[i].start();
				if(i==0)
				{
					hardDrives[i].runOperatingSystem();
				}
			}
		}
		
		
	}
}
